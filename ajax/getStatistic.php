<?php
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if($_REQUEST["DATE_ACTIVE_FROM"] && !$DB->IsDate(trim($_REQUEST["DATE_ACTIVE_FROM"]), "MM/DD/YYYY")) {
    return false;
}

if($_REQUEST["DATE_ACTIVE_TO"] && !$DB->IsDate(trim($_REQUEST["DATE_ACTIVE_TO"]), "MM/DD/YYYY")) {
    return false;
}

$APPLICATION->IncludeComponent(
    "uniplace:statistic",
    "ajax",
    [
        "DOCUMENTS_IBLOCK_ID" => 3,
        "PAYMENTS_IBLOCK_ID" => 4,
        "REPORTS_IBLOCK_ID" => 2
    ]
);