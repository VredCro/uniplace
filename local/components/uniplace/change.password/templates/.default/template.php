<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="main-wrapper change-pass">
    <div class="title">Profile</div>
    <?if($arResult["ERROR"]):?>
        <div class="error">
            <?= $arResult["ERROR"]?>
        </div>
    <?endif;?>
    <?if($arResult["SUCCESS"]):?>
        <div class="success">
            <?= $arResult["SUCCESS"]?>
        </div>
    <?else:?>
        <form name="form_auth" method="post" target="_top" action="">
            <div class="form-fields">
                <div class="field">
                    <div class="name">
                        Current Password
                    </div>
                    <div class="value">
                        <input class="input" type="password" name="CURRENT_PASSWORD" maxlength="255" value="">
                    </div>
                </div>
                <div class="field">
                    <div class="name">
                        New Password
                    </div>
                    <div class="value">
                        <input class="input" type="password" name="NEW_PASSWORD" maxlength="255" autocomplete="off">
                    </div>
                </div>
            </div>

            <input type="submit" class="btn btn-primary" name="Login" value="Change password">
        </form>
    <?endif;?>
</div>