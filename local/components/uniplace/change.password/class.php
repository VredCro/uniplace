<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class UniplaceChangePasswordComponent extends CBitrixComponent
{
    private function isUserPassword($userId, $password)
    {
        $userData = CUser::GetByID($userId)->Fetch();

        $salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));

        $realPassword = substr($userData['PASSWORD'], -32);
        $password = md5($salt . $password);

        return ($password == $realPassword);
    }

    public function executeComponent()
    {
        $user = new CUser();
        $userId = $user->GetID();

        if(!$userId) {
            LocalRedirect("/auth/");
            return false;
        }

        if ($_REQUEST["CURRENT_PASSWORD"] && $_REQUEST["NEW_PASSWORD"]) {
            $isUserPassword = $this->isUserPassword($userId, $_REQUEST["CURRENT_PASSWORD"]);

            if (!$isUserPassword) {
                $this->arResult["ERROR"] = "Incorrect User Password";
            } else {
                $success = $user->Update($userId, [
                    "PASSWORD"          => $_REQUEST["NEW_PASSWORD"],
                    "CONFIRM_PASSWORD"  => $_REQUEST["NEW_PASSWORD"],
                ]);

                if($success) {
                    $this->arResult["SUCCESS"] = "New password is set successfully.";
                } else {
                    $this->arResult["ERROR"] = "New password not set. <br>" . $user->LAST_ERROR;
                }
            }
        }
        $this->includeComponentTemplate();
    }

}