<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<div id="tabs">
    <ul>
        <? if ($arResult["REPORT"]): ?>
            <li><a href="#tabs-1">Report</a></li>
        <? endif; ?>
        <li><a href="#tabs-2">API client</a></li>
        <? if ($arResult["DOCUMENTS"]): ?>
            <li><a href="#tabs-3">Documents</a></li>
        <? endif; ?>
        <? if ($arResult["PAYMENTS"]): ?>
            <li><a href="#tabs-4">Payments</a></li>
        <? endif; ?>
    </ul>
    <? if ($arResult["REPORT"]): ?>
        <div id="tabs-1">
            <input type="text" data-range="true" data-date-format="mm/dd/yyyy" value="<?= date('m/01/Y') ?> - <?= date('m/t/Y') ?>"
                   data-multiple-dates-separator=" - " class="my-datepicker input"/>
            <div class="btn js-get-reports">
                Show
            </div>
            <div class="table">
                <div class="row first">
                    <div class="column">API method</div>
                    <div class="column">Calls</div>
                    <div class="column">Price, USD</div>
                </div>
                <? foreach ($arResult["REPORT"]["ITEMS"] as $item): ?>
                    <div class="row">
                        <div class="column"><?= $item["NAME"] ?></div>
                        <div class="column"><?= $item["PROPERTY_CALLS_VALUE"] ?></div>
                        <div class="column"><?= $item["PROPERTY_PRICE_VALUE"] ?></div>
                    </div>
                <? endforeach; ?>
                <div class="row last">
                    <div class="column">Summary</div>
                    <div class="column"><?= $arResult["REPORT"]["SUMMARY"]["CALLS"] ?></div>
                    <div class="column"><?= $arResult["REPORT"]["SUMMARY"]["PRICE"] ?></div>
                </div>
            </div>
        </div>
    <? endif; ?>
    <div id="tabs-2">
        <p>
            All Uniplace API calls should go through Uniplace client. It’s a Docker image. To run it <a
                    href="https://docs.docker.com/install/" target="_blank"> install Docker</a> on
            your server. Then execute
        </p>
        <div class="code">
            $ docker import /path/to/uniplace-client.tgz <br>
            $ docker run < port >:80 uniplace-client:latest
        </div>
        <p>
            Replace < port > with a port you want Uniplace client to run on. Then you can send API calls to
        </p>
        <div class="code">
            http://locallhost:< port >
        </div>
        <? if ($arResult["API"]): ?>
            <a href="<?= $arResult["API"] ?>" class="btn">
                Download API client
            </a>
        <? endif; ?>
    </div>
    <? if ($arResult["DOCUMENTS"]): ?>
        <div id="tabs-3">
            <input type="text" data-range="true" data-date-format="mm/dd/yyyy" value="<?= date('m/01/Y') ?> - <?= date('m/t/Y') ?>"
                   data-multiple-dates-separator=" - " class="my-datepicker input"/>
            <div class="btn js-get-documents">
                Show
            </div>
            <div class="table">
                <div class="row first">
                    <div class="column">Date</div>
                    <div class="column">Documents</div>
                </div>
                <? foreach ($arResult["DOCUMENTS"]["ITEMS"] as $item): ?>
                    <div class="row">
                        <div class="column"><?= $item["DATE"] ?></div>
                        <div class="column">
                            <a href="<?= $item["DOCUMENT"]["SRC"] ?>"
                               target="_blank"><?= $item["DOCUMENT"]["FILE_NAME"] ?></a>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    <? endif; ?>
    <? if ($arResult["PAYMENTS"]): ?>
        <div id="tabs-4">
            <input type="text" data-range="true" data-date-format="mm/dd/yyyy" value="<?= date('m/01/Y') ?> - <?= date('m/t/Y') ?>"
                   data-multiple-dates-separator=" - " class="my-datepicker input"/>
            <div class="btn js-get-payments">
                Show
            </div>
            <div class="table">
                <div class="row first">
                    <div class="column">Date</div>
                    <div class="column">Sum, USD</div>
                </div>
                <? foreach ($arResult["PAYMENTS"]["ITEMS"] as $item): ?>
                    <div class="row">
                        <div class="column"><?= $item["DATE"] ?></div>
                        <div class="column"><?= $item["PROPERTY_PRICE_VALUE"] ?></div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    <? endif; ?>
</div>