<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<? if ($arResult["REPORT"]): ?>
    <div class="table">
        <div class="row first">
            <div class="column">API method</div>
            <div class="column">Calls</div>
            <div class="column">Price, USD</div>
        </div>
        <? foreach ($arResult["REPORT"]["ITEMS"] as $item): ?>
            <div class="row">
                <div class="column"><?= $item["NAME"]?></div>
                <div class="column"><?= $item["PROPERTY_CALLS_VALUE"]?></div>
                <div class="column"><?= $item["PROPERTY_PRICE_VALUE"]?></div>
            </div>
        <? endforeach; ?>
        <div class="row last">
            <div class="column">Summary</div>
            <div class="column"><?= $arResult["REPORT"]["SUMMARY"]["CALLS"]?></div>
            <div class="column"><?= $arResult["REPORT"]["SUMMARY"]["PRICE"]?></div>
        </div>
    </div>
<? endif; ?>

<? if ($arResult["DOCUMENTS"]): ?>
    <div class="table">
        <div class="row first">
            <div class="column">Date</div>
            <div class="column">Documents</div>
        </div>
        <? foreach ($arResult["DOCUMENTS"]["ITEMS"] as $item): ?>
            <div class="row">
                <div class="column"><?= $item["DATE"] ?></div>
                <div class="column">
                    <a href="<?= $item["DOCUMENT"]["SRC"] ?>" target="_blank"><?= $item["DOCUMENT"]["FILE_NAME"]?></a>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>

<? if ($arResult["PAYMENTS"]): ?>
    <div class="table">
        <div class="row first">
            <div class="column">Date</div>
            <div class="column">Sum, USD</div>
        </div>
        <? foreach ($arResult["PAYMENTS"]["ITEMS"] as $item): ?>
            <div class="row">
                <div class="column"><?= $item["DATE"] ?></div>
                <div class="column"><?= $item["PROPERTY_PRICE_VALUE"]?></div>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>