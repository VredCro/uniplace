<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class UniplaceStatisticComponent extends CBitrixComponent
{
    private function getReports()
    {
        $arElements = $this->getItems(
            [
                "IBLOCK_ID" => $this->arParams["REPORTS_IBLOCK_ID"],
                "ACTIVE" => "Y",
            ],
            [
                "ID",
                "NAME",
                "PROPERTY_CALLS",
                "PROPERTY_PRICE",
                "DATE_ACTIVE_FROM"
            ]
        );

        $sumCalls = 0;
        $sumPrice = 0;

        foreach ($arElements as $arElement) {
            $sumCalls += (int)$arElement["PROPERTY_CALLS_VALUE"];
            $sumPrice += (int)$arElement["PROPERTY_PRICE_VALUE"];
        }

        $this->arResult["REPORT"]["SUMMARY"]["CALLS"] = $sumCalls;
        $this->arResult["REPORT"]["SUMMARY"]["PRICE"] = $sumPrice;

        return $arElements;
    }

    private function getApiClient()
    {
        global $USER;

        $arRes = CUser::GetList(
            $by,
            $desc,
            [
                "ID" => $USER->GetID()
            ],
            [
                "SELECT" => ["UF_API_CLIENT"]
            ]
        )->Fetch();

        if ($arRes["UF_API_CLIENT"]) {
            return CFile::GetPath($arRes["UF_API_CLIENT"]);
        }

        return false;
    }

    private function getDocuments()
    {
        $arElements = $this->getItems(
            [
                "IBLOCK_ID" => $this->arParams["DOCUMENTS_IBLOCK_ID"],
                "ACTIVE" => "Y",
            ],
            [
                "ID",
                "NAME",
                "IBLOCK_ID",
                "PROPERTY_DOCUMENT",
                "PROPERTY_PRICE",
                "DATE_ACTIVE_FROM"
            ]
        );

        foreach ($arElements as &$arElement) {
            $arElement["DOCUMENT"] = CFile::GetFileArray($arElement["PROPERTY_DOCUMENT_VALUE"]);
            $arElement["DATE"] = ConvertDateTime($arElement["DATE_ACTIVE_FROM"], "MM/DD/YYYY");
        }

        return $arElements;
    }

    private function getPayments()
    {
        $arElements = $this->getItems(
            [
                "IBLOCK_ID" => $this->arParams["PAYMENTS_IBLOCK_ID"],
                "ACTIVE" => "Y",
            ],
            [
                "ID",
                "NAME",
                "PROPERTY_PRICE",
                "DATE_ACTIVE_FROM"
            ]
        );

        foreach ($arElements as &$arElement) {
            $arElement["DATE"] = ConvertDateTime($arElement["DATE_ACTIVE_FROM"], "MM/DD/YYYY");
        }

        return $arElements;
    }

    private function getItems($arFilter = [], $arSelect = [])
    {
        global $USER;

        CModule::IncludeModule("iblock");

        $obEl = new CIBlockElement();
        $requestActiveFrom = $_REQUEST["DATE_ACTIVE_FROM"];
        $requestActiveTo = $_REQUEST["DATE_ACTIVE_TO"];

        $arFilter[">=DATE_ACTIVE_FROM"] = date('m/01/Y') . "00:00:01 am";
        $arFilter["<=DATE_ACTIVE_FROM"] = date('m/t/Y') . "11:59:59 pm";
        $arFilter["PROPERTY_USER"] = $USER->GetID();

        if($requestActiveFrom) {
            $requestActiveFrom .= "00:00:01 am";
        }

        if($requestActiveTo) {
            $requestActiveTo .= "11:59:59 pm";
        }

        if ($requestActiveFrom && $requestActiveTo) {
            unset($arFilter[">=DATE_ACTIVE_FROM"]);
            unset($arFilter["<=DATE_ACTIVE_FROM"]);
            array_push($arFilter, [
                "LOGIC" => "AND",
                [
                    ">=DATE_ACTIVE_FROM" => trim($requestActiveFrom)
                ],
                [
                    "<=DATE_ACTIVE_FROM" => trim($requestActiveTo)
                ]
            ]);
        } elseif ($requestActiveFrom) {
            $arFilter[">=DATE_ACTIVE_FROM"] = trim($requestActiveFrom);
        } elseif ($requestActiveTo) {
            $arFilter["<=DATE_ACTIVE_FROM"] = trim($requestActiveTo);
        }

        $dbRes = $obEl->GetList(
            [],
            $arFilter,
            false,
            false,
            $arSelect
        );

        $arElements = [];

        while ($arElement = $dbRes->Fetch()) {
            $arElements[$arElement["ID"]] = $arElement;
        }

        return $arElements;
    }

    public function executeComponent()
    {
        switch ($_REQUEST["TAB"]) {
            case "REPORT":
                $this->arResult["REPORT"]["ITEMS"] = $this->getReports();
                break;
            case "API":
                $this->arResult["API"] = $this->getApiClient();
                break;
            case "DOCUMENTS":
                $this->arResult["DOCUMENTS"]["ITEMS"] = $this->getDocuments();
                break;
            case "PAYMENTS":
                $this->arResult["PAYMENTS"]["ITEMS"] = $this->getPayments();
                break;
            default:
                $this->arResult["REPORT"]["ITEMS"] = $this->getReports();
                $this->arResult["API"] = $this->getApiClient();
                $this->arResult["DOCUMENTS"]["ITEMS"] = $this->getDocuments();
                $this->arResult["PAYMENTS"]["ITEMS"] = $this->getPayments();
                break;
        }

        $this->includeComponentTemplate();
    }

}