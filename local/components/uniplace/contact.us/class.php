<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Context;
use Bitrix\Main\Mail\Event;

class UniplaceContactUsComponent extends CBitrixComponent
{
    public function executeComponent()
    {
        $request = Context::getCurrent()->getRequest();

        $arEventFields = [
            "FIRST_NAME" => $request->get("first_name"),
            "LAST_NAME" => $request->get("last_name"),
            "WORK_EMAIL" => $request->get("work_email"),
            "PHONE" => $request->get("phone"),
            "COMPANY_NAME" => $request->get("company_name"),
            "COMPANY_WEBSITE" => $request->get("company_website"),
            "NUMBER_OF_EMPLOYEES" => $request->get("number_employees"),
            "COMPANY_INDUSTRY" => $request->get("company_industry"),
            "YOUR_DEPARTMENT" => $request->get("department"),
            "JOB_TITLE" => $request->get("job"),
            "WHY_DO_YOU_NEED" => $request->get("why")
        ];

        if ($_POST["first_name"]) {
            $res = Event::send(array(
                "EVENT_NAME" => $this->arParams["EVENT_NAME"],
                "LID" => "s1",
                "C_FIELDS" => $arEventFields
            ));

            $error = $res->getErrorMessages();

            if ($error) {
                $this->arResult["ERROR"] = $error;
            } else {
                $this->addInIBlock($arEventFields);
                LocalRedirect("/success/");
                die();
            }
        }

        $this->includeComponentTemplate();
    }

    private function addInIBlock($arFields) {
        CModule::IncludeModule("iblock");

        if(!$this->arParams["IBLOCK_ID"]) {
            return false;
        }

        $obElement = new CIBlockElement();

        $id = $obElement->Add([
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
            "NAME" => $arFields["WORK_EMAIL"],
            "PROPERTY_VALUES" => $arFields
        ]);

        return $id ? $id : false;
    }

}