<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="contact-us">
    <div class="title">
        Contact us
    </div>
    <? if (!$arResult["SUCCESS"]): ?>
        <div class="subtext">
            Consult our technical experts and learn how you can benefit from Uniplace API
        </div>
    <?endif;?>
    <?if($arResult["ERROR"]):?>
        <div class="error">
            <?= $arResult["ERROR"]?>
        </div>
    <?endif;?>
    <form class="form" method="post">
        <div class="field">
            <div class="name">First Name</div>
            <div class="value">
                <input type="text" required class="input" name="first_name">
            </div>
        </div>
        <div class="field">
            <div class="name">Last Name</div>
            <div class="value">
                <input type="text" required class="input" name="last_name">
            </div>
        </div>
        <div class="field full">
            <div class="name">Work Email</div>
            <div class="value">
                <input type="email" required class="input" name="work_email">
            </div>
        </div>
        <div class="field full">
            <div class="name">Phone</div>
            <div class="value">
                <input type="tel" required class="input" name="phone">
            </div>
        </div>
        <div class="field">
            <div class="name">Company Name</div>
            <div class="value">
                <input type="tel" required class="input" name="company_name">
            </div>
        </div>
        <div class="field">
            <div class="name">Company Website</div>
            <div class="value">
                <input type="tel" required class="input" name="company_website">
            </div>
        </div>
        <div class="field">
            <div class="name">Number of Employees</div>
            <div class="value">
                <input type="tel" required class="input" name="number_employees">
            </div>
        </div>
        <div class="field">
            <div class="name">Company Industry</div>
            <div class="value">
                <input type="tel" required class="input" name="company_industry">
            </div>
        </div>
        <div class="field">
            <div class="name">Your Department</div>
            <div class="value">
                <input type="tel" required class="input" name="department">
            </div>
        </div>
        <div class="field">
            <div class="name">Job title</div>
            <div class="value">
                <input type="tel" required class="input" name="job">
            </div>
        </div>
        <div class="field full">
            <div class="name">Why do you need Uniplace API</div>
            <div class="value">
                <textarea name="why" required class="input" rows="10"></textarea>
            </div>
        </div>
        <div class="field full">
            <input type="submit" class="btn btn-primary" name="contact-us" value="Send">
        </div>
    </form>
</div>