<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?>
<html>
<head>
    <? $APPLICATION->ShowHead(); ?>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title><? $APPLICATION->ShowTitle() ?></title>

    <script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery-3.4.1.min.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link href="<?= SITE_TEMPLATE_PATH ?>/js/air-datepicker-master/dist/css/datepicker.min.css" rel="stylesheet"
          type="text/css">
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/air-datepicker-master/dist/js/datepicker.min.js"></script>

    <script src="<?= SITE_TEMPLATE_PATH ?>/js/script.js"></script>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#FFFFFF">

<? $APPLICATION->ShowPanel() ?>

<header>
    <div class="container pos-rel">
        <? if ($APPLICATION->GetCurPage(false) === "/"): ?>
            <img src="<?= SITE_TEMPLATE_PATH ?>/images/rectangle.png" alt="">
            <img src="<?= SITE_TEMPLATE_PATH ?>/images/main-header.png" alt="" class="class-rect-2">
        <? else: ?>
            <img src="<?= SITE_TEMPLATE_PATH ?>/images/rectangle-small.png" alt="">
            <img src="<?= SITE_TEMPLATE_PATH ?>/images/main-header-small.png" alt="" class="class-rect-2">
        <? endif; ?>
        <div class="header<?= $APPLICATION->GetCurPage(false) !== "/" ? " small" : "" ?>">
            <div class="logo">
                <a href="<?= $USER->IsAuthorized() ? "/statistic/" : "/"?>">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/logo.svg" alt="">
                </a>
            </div>
            <div class="links">
                <? if ($APPLICATION->GetCurPage(false) !== "/api/"): ?>
                    <a href="/api/" class="link">
                        API documentation
                    </a>
                <? endif; ?>
                <? if (!$USER->IsAuthorized()): ?>
                    <a href="/auth/" class="button sign-in">
                        Sign in
                    </a>
                <?else:?>
                    <a href="/statistic/" class="link js-toggle-drop"><?= $USER->GetEmail()?></a>
                    <div class="dropdown">
                        <a href="/profile/" class="item">Profile</a>
                        <a href="/?logout=yes" class="item">Sign out</a>
                    </div>
                <? endif ?>
            </div>
            <? if ($APPLICATION->GetCurPage(false) === "/"): ?>
                <div class="wrapper">
                    <div class="text">
                        Build amazing products <br/>
                        with Uniplace API
                    </div>
                </div>
                <div class="wrapper">
                    <a class="button" href="/contact-us/">
                        Get in touch
                    </a>
                </div>
            <? endif; ?>
        </div>
    </div>
</header>

<div class="container main-container">