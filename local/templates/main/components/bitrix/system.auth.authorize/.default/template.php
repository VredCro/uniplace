<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="main-wrapper auth-form">
<?if($arResult["AUTH_SERVICES"]):?>
	<div class="title">Sign in</div>
<?endif?>
    <?
    ShowMessage($arParams["~AUTH_RESULT"]);
    ShowMessage($arResult['ERROR_MESSAGE']);
    ?>
	<form name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
		<input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="AUTH" />
		<?if (strlen($arResult["BACKURL"]) > 0):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?endif?>
		<?foreach ($arResult["POST"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
		<?endforeach?>

        <div class="form-fields">
            <div class="field">
                <div class="name">
                    Email
                </div>
                <div class="value">
                    <input class="input" type="text" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" />
                </div>
            </div>
            <div class="field">
                <div class="name">
                    Password
                </div>
                <div class="value">
                    <input class="input" type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" />
                </div>
            </div>
        </div>

        <input type="submit" class="btn btn-primary" name="Login" value="Sign in" />
	</form>
</div>

<script type="text/javascript">
<?if (strlen($arResult["LAST_LOGIN"])>0):?>
try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<?else:?>
try{document.form_auth.USER_LOGIN.focus();}catch(e){}
<?endif?>
</script>