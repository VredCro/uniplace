<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="documentation-list">
    <? foreach ($arResult["ITEMS"] as $key => $item): ?>
        <?
        if(!$item["~DETAIL_TEXT"] && !$item["~PREVIEW_TEXT"]) {
            continue;
        }
        ?>
        <div class="item" id="<?= $item["CODE"] ?>">
            <div class="title">
                <?= $item["NAME"] ?>
            </div>
            <div class="text">
				<?= $item["~DETAIL_TEXT"]  ? $item["~DETAIL_TEXT"] : $item["~PREVIEW_TEXT"]?>
            </div>
        </div>
    <? endforeach; ?>
</div>