<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="documentation-list">
    <div class="column">
        <? foreach ($arResult["ITEMS"] as $key => $item): ?>
            <?
             if($key % 2 !== 0) {
                 continue;
             }
            ?>
            <div class="item">
                <div class="title">
                    <?= $item["NAME"] ?>
                </div>
                <div class="text">
                    <?= $item["PREVIEW_TEXT"] ?>
                </div>
                <a href="/api/#<?= $item["CODE"] ?>" class="link">
                    Documentation
                </a>
            </div>
        <? endforeach; ?>
    </div>
    <div class="column">
        <? foreach ($arResult["ITEMS"] as $key => $item): ?>
            <?
            if($key % 2 === 0) {
                continue;
            }
            ?>
            <div class="item">
                <div class="title">
                    <?= $item["NAME"] ?>
                </div>
                <div class="text">
                    <?= $item["PREVIEW_TEXT"] ?>
                </div>
                <a href="/api/#<?= $item["CODE"] ?>" class="link">
                    Documentation
                </a>
            </div>
        <? endforeach; ?>
    </div>
</div>