<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

</div>
<footer>
    <div class="container">
        <div class="items-wrap">
            <div class="item">
                © 2019 Deepdive Software Limited
            </div>
            <div class="item">
                <a href="mailto:support@uniplace.com.cy">support@uniplace.com.cy</a>
            </div>
            <div class="item">
                <a href="">cookie policy </a>
            </div>
            <div class="item">βeta</div>
        </div>
    </div>
</footer>

</body>
</html>