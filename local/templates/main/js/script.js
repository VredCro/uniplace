$(document).ready(function () {
    $( "#tabs" ).tabs();

    $("body")
        .on("click", ".js-get-reports", function () {
            let date = $("#tabs-1 .my-datepicker").val().split("-");

            $.ajax({
                url: "/ajax/getStatistic.php",
                type: "POST",
                data: {
                    TAB: "REPORT",
                    DATE_ACTIVE_FROM: date[0],
                    DATE_ACTIVE_TO: date[1]
                },
                success: function (response) {
                    if(!response) {
                        return ;
                    }
                    $("#tabs-1 .table").replaceWith(response);
                }
            });
        })
        .on("click", ".js-get-documents", function () {
            let date = $("#tabs-3 .my-datepicker").val().split("-");

            $.ajax({
                url: "/ajax/getStatistic.php",
                type: "POST",
                data: {
                    TAB: "DOCUMENTS",
                    DATE_ACTIVE_FROM: date[0],
                    DATE_ACTIVE_TO: date[1]
                },
                success: function (response) {
                    if(!response) {
                        return ;
                    }
                    $("#tabs-3 .table").replaceWith(response);
                }
            });
        })
        .on("click", ".js-get-payments", function () {
            let date = $("#tabs-4 .my-datepicker").val().split("-");

            $.ajax({
                url: "/ajax/getStatistic.php",
                type: "POST",
                data: {
                    TAB: "PAYMENTS",
                    DATE_ACTIVE_FROM: date[0],
                    DATE_ACTIVE_TO: date[1]
                },
                success: function (response) {
                    if(!response) {
                        return ;
                    }
                    $("#tabs-4 .table").replaceWith(response);
                }
            });
        })
        .on("click", ".js-toggle-drop", function (e) {
           e.preventDefault();
           $(".dropdown").toggleClass("showed");
        });

    $.fn.datepicker.language['en'] =  {
        days: ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
        daysShort: ['Sun','Mon','Tues','Wed','Thurs','Fri','Sat'],
        daysMin: ['Sun','Mon','Tues','Wed','Thurs','Fri','Sat'],
        months: ['January','February','March','April','May','June','July','August','September','October','November','December'],
        monthsShort: ['Jan','Feb','Mar','Apr','May','Ju','Jul','Aug','Sep','Oct','Nov','Dec'],
        today: 'Today',
        clear: 'Clear',
        dateFormat: 'mm/dd/yyyy',
        timeFormat: 'hh:ii',
        firstDay: 1
    };


    $(document).ready(function () {
        $(".my-datepicker").datepicker({
            language: 'en'
        });
    })
})