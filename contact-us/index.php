<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Contact us");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Contact us");
?>
    <div class="container">
        <div class="main-wrapper">
            <?
                $APPLICATION->IncludeComponent(
                    "uniplace:contact.us",
                    ".default",
                    [
                        "EVENT_NAME" => "CONTACT_US",
                        "IBLOCK_ID" => 5
                    ]
                );
            ?>
        </div>
    </div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>