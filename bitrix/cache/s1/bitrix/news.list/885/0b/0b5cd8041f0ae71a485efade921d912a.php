<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001564826413';
$dateexpire = '001600826413';
$ser_content = 'a:2:{s:7:"CONTENT";s:5153:"
<div class="documentation-list">
                    <div class="item" id="overview">
            <div class="title">
                Overview            </div>
            <div class="text">
				<p>All Uniplace API calls should go through Uniplace client. You can download it here. It’s a Docker image. To run it install Docker on your server. Then execute</p>
<div class="code">
 $ docker import /path/to/uniplace-client.tgz <br />
 $ docker run < port >:80 uniplace-client:latest
</div>
<p>
Replace <port> with a port you want Uniplace client to run on. Then you can send API calls to
</p>

<div class="code">
http://locallhost:< port >
</div>

<p>
Uniplace API conforms to JSON-RPC 2.0 specification. All API calls are POST requests. All requests follow the standard JSON-RPC format and include 4 variables in the data object: id, jsonrpc, method and params. Response includes id, jsonrpс and result or error.
</p>
<p>
API client is custom built for your account with your credentials. Authentication is automatic.
</p>            </div>
        </div>
                    <div class="item" id="links-placement">
            <div class="title">
                Links placement            </div>
            <div class="text">
				The module is intended for quick placement of links purchased by the system users on provided advertising platforms. <br>
This module allows increasing a link weight of a website by placing links automatically at websites of third parties that leads to an increase in organic traffic from search engines. <br>

<div class="subtitle">Request</div>

<pre class="code">
{
    "id": 1,
    "jsonrpc": "2.0",
    "method": "LinksPlacement",
    "params": {
        "param1": param1,
        "param2": param2
    }
}
</pre>

<div class="subtitle">Response</div>
<pre class="code">
{
    "id": 1,
    "jsonrpc": "2.0",
    "result": {}
}
</pre>

            </div>
        </div>
                    <div class="item" id="user-behaviour-simulation">
            <div class="title">
                User behaviour simulation            </div>
            <div class="text">
				The module is intended for quick placement of links purchased by the system users on provided advertising platforms. <br>
This module allows increasing a link weight of a website by placing links automatically at websites of third parties that leads to an increase in organic traffic from search engines. <br>

<div class="subtitle">Request</div>

<pre class="code">
{
    "id": 1,
    "jsonrpc": "2.0",
    "method": "LinksPlacement",
    "params": {
        "param1": param1,
        "param2": param2
    }
}
</pre>

<div class="subtitle">Response</div>
<pre class="code">
{
    "id": 1,
    "jsonrpc": "2.0",
    "result": {}
}
</pre>

            </div>
        </div>
                    <div class="item" id="links-generation">
            <div class="title">
                Links generation            </div>
            <div class="text">
				The module is intended to automatically generate anchors of links for a search query. Several sources are used to obtain a required number of anchors (search engine results, landing page).

This module allows increasing productivity of Internet marketing specialists due to automation of routine tasks on selection of texts of links for promotion of sites.

A JSON RPC compatible software client is required.            </div>
        </div>
                    <div class="item" id="reporting">
            <div class="title">
                Reporting            </div>
            <div class="text">
				Monitoring of system performance is required for timely localization of technical problems and automatic creation of technical and financial reports describing the current state of the system. The data to create reports is collected from time to time by separate algorithms, the launch rate of the ollector depends on speed of data change.

The module allows reducing the time and costs of system maintenance by detecting technical problems in the early stages of their occurrence.

A JSON RPC compatible software client is required. All monitoring servers should have an operating system supported by the GO compiler. The price depends on the amount of resources used. Contact us for a preliminary calculation.            </div>
        </div>
                    <div class="item" id="call-center-automation">
            <div class="title">
                Call-center automation            </div>
            <div class="text">
				The module is intended for arrangement of interaction between the manager and the client by means of storing data on clients and history of relationships with them, establishing and improving business processes and further analysis of results.

The module allows increasing the productivity of telemarketing specialists up to 100% due to control and automation of the processes of selection and evaluation of clients for calling based on analysis of the conversion funnel.

A JSON RPC compatible software client is required.            </div>
        </div>
    </div>";s:4:"VARS";a:2:{s:8:"arResult";a:7:{s:2:"ID";s:1:"1";s:14:"IBLOCK_TYPE_ID";s:4:"main";s:13:"LIST_PAGE_URL";s:0:"";s:15:"NAV_CACHED_DATA";N;s:4:"NAME";s:17:"API documentation";s:7:"SECTION";b:0;s:8:"ELEMENTS";a:6:{i:0;s:1:"8";i:1;s:1:"1";i:2;s:1:"4";i:3;s:1:"5";i:4;s:1:"6";i:5;s:1:"7";}}s:18:"templateCachedData";a:3:{s:13:"additionalCSS";s:63:"/local/templates/main/components/bitrix/news.list/api/style.css";s:9:"frameMode";b:1;s:17:"__currentCounters";a:1:{s:28:"bitrix:system.pagenavigation";i:1;}}}}';
return true;
?>