<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Statistic");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Statistic");
?>
    <div class="container">
        <div class="main-wrapper">
            <?
            $APPLICATION->IncludeComponent(
                "uniplace:statistic",
                "",
                [
                    "DOCUMENTS_IBLOCK_ID" => 3,
                    "PAYMENTS_IBLOCK_ID" => 4,
                    "REPORTS_IBLOCK_ID" => 2
                ]
            )
            ?>
        </div>
    </div>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>